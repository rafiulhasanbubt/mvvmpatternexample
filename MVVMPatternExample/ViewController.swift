//
//  ViewController.swift
//  MVVMPatternExample
//
//  Created by rafiul hasan on 10/24/19.
//  Copyright © 2019 rafiul hasan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var movieDataArray = [MovieViewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.getData()
        tableView.reloadData()
    }
    
    func getData(){
        Service.shareInstance.getMovieData { (movies, error) in
            if (error == nil) {
                self.movieDataArray = movies?.map({return MovieViewModel(movie: $0)}) ?? []
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movieDataArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        let MVM = movieDataArray[indexPath.row]
        cell.textLabel?.text = MVM.artistName 
        cell.detailTextLabel?.text = MVM.trackName 
        return cell
    }
    
}

