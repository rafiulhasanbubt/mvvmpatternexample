//
//  MovieModel.swift
//  MVVMPatternExample
//
//  Created by rafiul hasan on 10/24/19.
//  Copyright © 2019 rafiul hasan. All rights reserved.
//

import UIKit

class ResultsModel: Codable {
    var results = [MovieModel]()
    
    init(results: [MovieModel]){
        self.results = results
    }
}

class MovieModel: Codable {
    var artistName: String
    var trackName: String
    
    init(artistName: String, trackName: String) {
        self.artistName = artistName
        self.trackName = trackName
    }

}

