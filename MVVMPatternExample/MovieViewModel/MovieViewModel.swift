//
//  MovieViewModel.swift
//  MVVMPatternExample
//
//  Created by rafiul hasan on 10/24/19.
//  Copyright © 2019 rafiul hasan. All rights reserved.
//

import UIKit

class MovieViewModel: NSObject {
    var artistName: String
    var trackName: String
    
    init(movie: MovieModel) {
        self.artistName = movie.artistName
        self.trackName = movie.trackName
    }

}
