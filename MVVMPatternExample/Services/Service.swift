//
//  Service.swift
//  MVVMPatternExample
//
//  Created by rafiul hasan on 10/24/19.
//  Copyright © 2019 rafiul hasan. All rights reserved.
//

import UIKit

class Service: NSObject {
    
    static let shareInstance = Service()
    
    func getMovieData(completion: @escaping([MovieModel]?, Error?) -> ()){
        let baseURL = "https://itunes.apple.com/search?media=music&term=bollywood"
        guard let url = URL(string: baseURL) else { return }
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let err = error {
                completion(nil, err)
                print(err.localizedDescription)
            } else {
                guard let data = data else { return }
                do {
                    var movieArray = [MovieModel]()
                    let jsonData = try JSONDecoder().decode(ResultsModel.self, from: data)
                    print(jsonData)
                    
                    for movie in jsonData.results {
                        movieArray.append(MovieModel(artistName: movie.artistName, trackName: movie.trackName))
                    }
                    
                } catch let jsonErr {
                    print(jsonErr.localizedDescription)
                }
            }
        }
        task.resume()
    }

}
